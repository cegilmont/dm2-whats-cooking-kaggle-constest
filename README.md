# What's Cooking

Predict cuisine based on ingredients using a Random Forest, Multinomial NaiveBayes, Multinomial Logistic Regression and XGBoost classifiers. <br />
Data Mining 2 Kaggle Contest. <br />
May 2017.