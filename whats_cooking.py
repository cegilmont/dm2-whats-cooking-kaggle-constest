import time
import pandas as pd
from collections import Counter
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from xgboost import XGBClassifier

# load data (JSON to DataFrame)
train = pd.read_json('C:/Users/cg/Desktop/DM2/Project/train.json')
test = pd.read_json('C:/Users/cg/Desktop/DM2/Project/test.json')

cuisine_list = []
ingredient_list = []

# create a list of all cuisines mentioned, same for ingredients
for index, row in train.iterrows():
	cuisine_list.append(row['cuisine'])
	ingredient_list.extend(row['ingredients'])

# create a list of tuples, each cuisine and how many times it is mentioned
# same for ingredients
cuis_count = Counter(cuisine_list)
cuis_grouped = cuis_count.most_common()

ing_count = Counter(ingredient_list)
ing_grouped = ing_count.most_common()

# output top 25 cuisines and ingredients
# print(cuis_grouped[:25])
# print(ing_grouped[:25])

# output total number of cuisines and ingredients
# print(len(cuis_grouped))
# print(len(ing_grouped))

#=========================================
train['ingredient_list'] = [','.join(z).strip() for z in train['ingredients']]

ingredients = train['ingredient_list']

tfidf = TfidfVectorizer(stop_words='english')
train_tfidf_matrix = tfidf.fit_transform(ingredients)

test['ingredient_list'] = [','.join(z).strip() for z in test['ingredients']]

# CSV names
class_names = ['randomForest10', 'randomForest20',
                        'randomForest50', 'randomForest100',
						'multinomialNaiveBayes', 'multinomialLogisticRegression',
						'xgboost']

# classifiers
randomForest10= RandomForestClassifier(n_estimators = 10)
randomForest20= RandomForestClassifier(n_estimators = 20)
randomForest50= RandomForestClassifier(n_estimators = 50)
randomForest100= RandomForestClassifier(n_estimators = 500)
multinomialNaiveBayes = MultinomialNB()
multinomialLogisticRegression = LogisticRegression()
xgboost = XGBClassifier()

# classifier list to iterate through
clf = [randomForest10, randomForest20,
         randomForest50, randomForest100,
		 multinomialNaiveBayes, multinomialLogisticRegression,
		 xgboost]

for (classifier, name)  in zip(clf, class_names):
	start = time.time()
	classifier.fit(train_tfidf_matrix, train['cuisine'])

	test_tfidf_matrix = tfidf.transform(test['ingredient_list'])
	test_cuisines = classifier.predict(test_tfidf_matrix)

	test['cuisine'] = test_cuisines

	# save to CSV
	test[['id' , 'cuisine' ]].to_csv(name + "_submission.csv", index=False)
	end = time.time()
	print(name, " ", end-start)




